<?php 
	include('includes/header-meta.php');
	include('includes/sidebar-left.php');
 ?>
 <div class="app-content content">
 	<?php include('includes/header.php'); ?>
 	<div class="content-wrapper">
 	 	<div class="card min-height-3">
			<div class="card-header border-bottom p-1">
				<h4 class="card-title">Chi tiết nhân viên</h4>
			</div>
			<div class="card-content mt-2">
				<div class="row m-2">
					<?php include('includes/page/tbl_chitietnhanvien.php'); ?>
				</div>
			</div> <!-- card-content -->
		</div>
	</div> <!-- end content-wrapper -->
	 </div>
	 <div class="sidenav-overlay"></div>
	 <div class="drag-target"></div>
 </div>
	 <?php include('includes/footer.php'); ?>
