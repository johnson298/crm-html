
      <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
   <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
         <li class="nav-item mr-auto">
            <a class="navbar-brand">
               <div class="brand-logo"></div>
               <h2 class="brand-text mb-0">Itsweb</h2>
            </a>
         </li>
         <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="icon-x d-block d-xl-none font-medium-4 primary toggle-icon feather icon-disc"></i><i class="toggle-icon icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary feather" data-ticon="icon-disc"></i></a></li>
      </ul>
   </div>
   <div class="shadow-bottom" ></div>
   <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         <li class="nav-item">
            <a href="tong_quan.php"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Tổng quan</span></a>
         </li>
         <li class="navigation-header"><span>Khách hàng</span>
         </li>
         <li class="nav-item"><a href="khach_hang.php"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Email">Khách hàng</span></a>
         </li>
         <li class=" navigation-header"><span>Dự án</span>
         </li>
         <li class="nav-item"><a href="san_pham.php"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="Email">Sản phẩm</span></a>
         </li>
         <li class="nav-item"><a href="danh_sach_don_hang.php"><i class="feather icon-shopping-cart"></i><span class="menu-title" data-i18n="Email">Đơn hàng</span></a>
         </li>
         <li class=" navigation-header"><span>Marketing</span>
         </li>
         <li class="nav-item"><a href="javavoid:(0)"><i class="feather icon-zap"></i><span class="menu-title" data-i18n="Email">Chiến dịch</span></a>
         </li>
         <li class="nav-item"><a href="javavoid:(0)"><i class="feather icon-cloud"></i><span class="menu-title" data-i18n="Email">Chăm sóc KH</span></a>
         </li>
         <li class=" navigation-header"><span>Nhân sự</span>
         </li>
         <li class="nav-item"><a href="nhan_vien.php"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Email">Nhân viên</span></a>
         </li>
         <li class="nav-item"><a href="phong_ban.php"><i class="feather icon-square"></i><span class="menu-title" data-i18n="Email">Phòng ban</span></a>
         </li>
      </ul>
      <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
         <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
      </div>
      <div class="ps__rail-y" style="top: 0px; height: 286px; right: 0px;">
         <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 35px;"></div>
      </div>
   </div>
</div>
<!-- END: Main Menu-->