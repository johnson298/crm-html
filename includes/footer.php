

<div id="warning-delete" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">          
                <h4 class="modal-title">Bạn có chắc xóa?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Bạn có thực sự muốn xóa những hồ sơ này? Quá trình này không thể được hoàn tác.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-danger">Xóa</button>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
 <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT © 2019<a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">Pixinvent,</a>All rights Reserved</span><span class="float-md-right d-none d-md-block">Hand-crafted &amp; Made with<i class="feather icon-heart pink"></i></span>
  <button class="btn btn-primary btn-icon scroll-top waves-effect waves-light" type="button"><i class="feather icon-arrow-up"></i></button>
</p>
</footer>
<!-- END: Footer-->
<!-- BEGIN: Vendor JS-->
<script src="./app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="./app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="./app-assets/js/core/app-menu.js"></script>
<script src="./app-assets/js/core/app.js"></script>
<script src="./app-assets/js/scripts/components.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="./app-assets/js/scripts/datatables/datatable.js"></script>
<!-- END: Page JS-->'

<script src="./app-assets/vendors/js/extensions/dropzone.min.js"></script>
<script src="./app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>

<script>
    $(document).ready(function() {
        $('#remove-sorting').removeClass('sorting_asc').removeClass('sorting_desc');
        $('.remove-sorting').removeClass('sorting_asc').removeClass('sorting_desc');
        $('#remove-sorting').on('click', function(event) {
            $(this).removeClass('sorting_desc').removeClass('sorting_asc')
        });

        $( ".remove-sorting" ).each(function(index) {
            $(this).on("click", function(){
                $(this).removeClass('sorting_desc').removeClass('sorting_asc')
            });
        });
    });
</script>
</body>
</html>