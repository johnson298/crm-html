<div class="card min-height-3">
	<div class="card-header border-bottom p-1">
		<h4 class="card-title">Nhân viên</h4>
	</div>
	<div class="card-content mt-2">
		<div class=" m-1">
			<div class="row">
				<div class="col-4">
					<?php include('tbl_treeview.php'); ?>
				</div>
				<div class="col-8">
					<h3 class="text-center p-1 bg-info rounded text-white">Danh sách người dùng HOBASOFT</h3>
					<table class="table  dataTable no-footer" id="_0" role="">
						<thead>
							<tr role="row">
								<th id="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" ></th>
								<th class="sorting_asc" >STT</th>
								<th class="sorting" >HỌ TÊN</th>
								<th class="sorting" >TÊN ĐĂNG NHẬP</th>
								<th class="sorting" >ĐƠN VỊ</th>
								<th class="sorting" >NHÓM QUYỀN</th>
								<th class="sorting" >VAI TRÒ</th>
								<th class="sorting" >THAO TÁC</th>
							</tr>
						</thead>
						<tbody>
							<tr role="row" class="odd">
								<th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
								<th class="sorting_1">1</th>
								<td class="sorting_1">
									<a href="chi_tiet_nhan_vien.php" class="text-decoration">Adminstrator</a>
								</td>
								<td>Admin</td>
								<td>
									<p>Phòng giám đốc</p>
								</td>
								<td>
									admin
								</td>
								<td>Lãnh đạo</td>
								<td>
									<div class="action-kh d-flex justify-content-around">
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editEmployees">
												<i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
											</button>
										</div>
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="">
												<i data-toggle="tooltip" title="hoàn thành" class="fa fa-check"></i>
											</button>
										</div>
										<div class="fonticon-wrap text-center">
                                          <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                             <i data-toggle="tooltip" title="" class="feather icon-trash-2 text-dark" data-original-title="xóa"></i>
                                          </a>
                                         </div>
									</div>
								</td>
							</tr>
							<tr role="row" class="even">
								<th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
								<th class="sorting_1">2</th>
								<td class="sorting_1">
									<a href="chi_tiet_nhan_vien.php" class="text-decoration">Son dep zai</a>
								</td>
								<td>Sondepzai</td>
								<td>
									<p>Phòng kinh doanh Mỹ</p>
								</td>
								<td>Kinh doanh
								</td>
								<td>Nhân viên</td>
								<td>
									<div class="action-kh d-flex justify-content-around">
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editEmployees">
												<i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
											</button>
										</div>
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="">
												<i data-toggle="tooltip" title="hoàn thành" class="fa fa-check"></i>
											</button>
										</div>
										<div class="fonticon-wrap text-center">
                                          <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                             <i data-toggle="tooltip" title="" class="feather icon-trash-2 text-dark" data-original-title="xóa"></i>
                                          </a>
                                         </div>
									</div>
								</td>
							</tr>
							<tr role="row" class="even">
								<th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
								<th class="sorting_1">3</th>
								<td class="sorting_1">
									<a href="chi_tiet_nhan_vien.php" class="text-decoration">Son dep zai</a>
								</td>
								<td>Sondepzai</td>
								<td>
									<p>Phòng kinh doanh Mỹ</p>
								</td>
								<td>Kinh doanh
								</td>
								<td>Nhân viên</td>
								<td>
									<div class="action-kh d-flex justify-content-around">
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editEmployees">
												<i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
											</button>
										</div>
										<div class="fonticon-wrap">
											<button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="">
												<i data-toggle="tooltip" title="hoàn thành" class="fa fa-check"></i>
											</button>
										</div>
										<div class="fonticon-wrap text-center">
                                          <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                             <i data-toggle="tooltip" title="" class="feather icon-trash-2 text-dark" data-original-title="xóa"></i>
                                          </a>
                                         </div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div> <!-- end row -->
		</div>
		
	</div>
</div>

<div class="modal-hid">
	<div class="modal fade" id="editEmployees" style="padding-right: 16px; padding-left: 16px;" aria-modal="true">
       <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
                </button>
             </div>
             <div class="modal-body">
                <div class="card m-0">
                   <div class="card-header">
                      <h4 class="card-title">Chỉnh sửa nhân viên</h4>
                   </div>
                   <div class="card-content">
                      <div class="card-body">
                         <form class="form form-horizontal">
                            <div class="form-body">
                               <div class="row">
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Họ tên</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="text" value="Adminstrator" class="form-control" id="">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Tên đăng nhập</span>
                                        </div>
                                        <div class="col-md-8"><input type="text" value="admin" class="form-control" id="">
                                        </div>
                                     </div>
                                  </div>
                                  
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Đơn vị</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">phòng giám đốc</option>
                                              <option value="Chỉnh sửa">phòng kinh doanh HN</option>
                                              <option value="Xóa">phòng kinh doanh Mỹ</option>
                                              <option value="Xóa">Phòng kinh doanh HCM</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Nhóm quyền</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">admin</option>
                                              <option value="Chỉnh sửa">kinh doanh</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div><div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Vai trò</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">lãnh đạo</option>
                                              <option value="Chỉnh sửa">nhân viên</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Lưu</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                               </div>
                            </div>
                         </form>
                      </div>
                      <!-- end card-body -->
                   </div>
                </div>
                <!-- end card -->
             </div>
             <!-- end modal body -->
          </div>
          <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
      </div>
</div>
                                                      