
<section>
  <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title">Khách hàng</h4>
            </div>
            <div class="card-content">
               <div class="card-body card-dashboard">
                  <p class="card-text">DataTables has most features enabled by default.</p>
                  <div class="table-responsive">
                     <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4">

                        <div class="row pt-1 pb-1 d-flex justify-content-end">
                          <div class="actions action-btns d-flex justify-content-end">
                             
                             <div class="dt-buttons btn-group"><button data-toggle="modal" data-target="#addNewCustomer" class="btn btn-outline-primary waves-effect waves-light" tabindex="0" aria-controls="DataTables_Table_0"><span><i class="feather icon-plus"></i>Tạo mới khách hàng</span></button> </div>
                          </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-12">
                              <table class="table zero-configuration dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                 <thead>
                                    <tr role="row">
                                       <th id="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" ></th>
                                       <th class="sorting_asc">STT</th>
                                       <th class="sorting " >MÃ KHÁCH HÀNG</th>
                                       <th class="sorting " t>TÊN KH</th>
                                       <th class="sorting" >SĐT</th>
                                       <th class="sorting" >EMAIL</th>
                                       <th class="sorting " >ĐỊA CHỈ</th>
                                       <th class="sorting " >CẤP KHÁCH HÀNG</th>
                                       <th class="sorting"  >SỐ LẦN CHĂM SÓC</th>
                                       <th class="sorting " >SỐ LẦN HỖ TRỢ</th>
                                       <th class="sorting_asc" >CÔNG NỢ</th>
                                       <th class="sorting" >HÀNH ĐỘNG</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr role="row" class="odd">
                                      <td><input type="checkbox" name="checkAll" class="checkSingle" ></td>
                                       <td class="sorting_1">1</td>
                                       <td>243531</td>
                                       <td>JohnSon</td>
                                       <td>09876</td>
                                       <td>abc@gmail.com</td>
                                       <td>Cầu Giấy - Hà Nội</td>
                                       <td>doanh nghiệp</td>
                                       <td>10</td>
                                       <td>1000</td>
                                       <td>0</td>
                                       <td>
                                         <div class="action-kh d-flex justify-content-around">
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editCustomer">
                                               <i class="fa fa-pencil" data-toggle="tooltip" title="chỉnh sửa"></i>
                                            </button>
                                           </div>
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#viewCustomer">
                                               <i class="fa fa-eye" data-toggle="tooltip" title="xem"></i>
                                            </button>
                                           </div>
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="tooltip" title="chuyển thành đối tác" data-target="#">
                                              <i class="fa fa-retweet"></i>
                                            </button>
                                           </div>
                                         </div>
                                       </td>
                                    </tr>
                                    <tr role="row" class="even">
                                      <td><input type="checkbox" name="checkAll" class="checkSingle" ></td>
                                       <td class="sorting_1">2</td>
                                       <td>12323</td>
                                       <td>Peter</td>
                                       <td>09876</td>
                                       <td>abc@gmail.com</td>
                                       <td>Cầu Giấy - Hà Nội</td>
                                       <td>doanh nghiệp</td>
                                       <td>10</td>
                                       <td>1000</td>
                                       <td>0</td>
                                       <td>
                                         <div class="action-kh d-flex justify-content-around">
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editCustomer">
                                               <i class="fa fa-pencil" data-toggle="tooltip" title="chỉnh sửa"></i>
                                            </button>
                                           </div>
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#viewCustomer">
                                               <i class="fa fa-eye" data-toggle="tooltip" title="xem"></i>
                                            </button>
                                           </div>
                                           <div class="fonticon-wrap">
                                            <button class="border-0 background-0 cursor-pointer" data-toggle="tooltip" title="chuyển thành đối tác" data-target="#">
                                              <i class="fa fa-retweet"></i>
                                            </button>
                                           </div>
                                         </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  </div> <!-- end row -->

</section>
<div class="modal-hid">
    <div class="modal fade text-left" id="editCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" style=" padding-right: 16px;" aria-modal="true">
     <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title card-title text-center font-weight-bold with-100">Chỉnh sửa khách hàng : johnShep</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
           </div>
           <div class="card-content">
             <div class="modal-body">
                <div class="row">
                  <div class="col-12">
                      <div class="card p-1">
                          <div class="card-header">
                          </div>
                          <div class="card-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="thongtin-tab" data-toggle="tab" href="#thongtin" role="tab" aria-controls="thongtin" aria-selected="true">THÔNG TIN CHUNG</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="donhang-tab" data-toggle="tab" href="#donhang" role="tab" aria-controls="donhang" aria-selected="false">ĐƠN HÀNG</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="chamsoc-tab" data-toggle="tab" href="#chamsoc" role="tab" aria-controls="chamsoc" aria-selected="false">CHĂM SÓC</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="hotro-tab" data-toggle="tab" href="#hotro" role="tab" aria-controls="hotro" aria-selected="false">HỖ TRỢ</a>
                              </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">

                              <div class="tab-pane fade show active" id="thongtin" role="tabpanel" aria-labelledby="thongtin-tab">
                                <div class="table-responsive pt-2 pr-2 pl-2 pb-0">
                                    <div class="row mb-1">

                                      <div class="col-12">
                                        <div class="row">
                                    
                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Tên khách hàng</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập tên dịch vụ">
                                              </div> <!-- end col-7 -->
                                            </div>
                                            
                                          </div> <!-- end col-6 -->

                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Cấp khách hàng</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <select class="form-control" id="basicSelect">
                                                    <option>doanh nghiệp</option>
                                                    <option>cá nhân</option>
                                                </select>
                                              </div> <!-- end col-7 -->
                                            </div>
                                          </div> <!-- end col-6 -->

                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Điện thoại</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <input type="number" class="form-control" placeholder="nhập số điện thoại" value="0912837">
                                              </div> <!-- end col-7 -->
                                            </div>
                                          </div> <!-- end col-6 -->

                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Email</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập email" value="abc@gmail.com">
                                              </div> <!-- end col-7 -->
                                            </div>
                                          </div> <!-- end col-6 -->

                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Địa chỉ</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập tên dịch vụ">
                                              </div> <!-- end col-7 -->
                                            </div>
                                          </div> <!-- end col-6 -->

                                          <div class="col-6 p-1">
                                            <div class="row">
                                              <div class="col-5">
                                                <label for="">
                                                  <strong>Ghi chú</strong>
                                                </label>
                                              </div> <!-- end col-5 -->
                                              <div class="col-7">
                                                <textarea class="form-control" id="" rows="3" placeholder="nhập ghi chú">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi aliquid est.</textarea>
                                              </div> <!-- end col-7 -->
                                            </div>
                                          </div> <!-- end col-6 -->
                                        </div> <!-- end row -->

                                      </div> <!-- end col-12 -->

                                    </div> <!-- end col-6 -->
                                    <div class="row d-flex justify-content-end">
                                      
                                      <button type="button" class="btn btn-success mr-1 mb-1 waves-effect waves-light" data-dismiss="modal">
                                        <i class="feather icon-check"></i>
                                      lưu thay đổi</button>
                                    </div>
                                </div>
                              </div> <!-- end tab-pane -->

                              <div class="tab-pane fade" id="donhang" role="tabpanel" aria-labelledby="donhang-tab">
                                <table class="table zero-configuration dataTable no-footer" id="_0" role="grid" aria-describedby="_0_info">
                                   <thead>
                                      <tr role="row">
                                         <th class="sorting_asc" >STT</th>
                                         <th class="sorting" >MÃ ĐƠN HÀNG</th>
                                         <th class="sorting" >DỊCH VỤ</th>
                                         <th class="sorting" >-NGÀY ĐĂNG KÝ<br>- NGÀY HẾT HẠN</th>
                                         <th class="sorting" >TT DỊCH VỤ</th>
                                         <th class="sorting" >TÔNG TIỀN</th>
                                         <th class="sorting" >TT THANH TOÁN</th>
                                         <th class="sorting" >HÀNH ĐỘNG</th>
                                      </tr>
                                   </thead>
                                   <tbody>
                                      <tr role="row" class="odd">
                                         <th class="sorting_1">1</th>
                                         <td class="sorting_1">itsweb</td>
                                         <td>Hosting - 12gb</td>
                                         <td>
                                            <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                            <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                         </td>
                                         <td>
                                            <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">chưa hết hạn</button><br>
                                            <i>(còn 12 ngày)</i>
                                         </td>
                                         <td>120000</td>
                                         <td>
                                            <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">đã thanh toán</button>
                                         </td>
                                         <td>
                                            <select class="text-center custom-select form-control" id="location1" name="location">
                                               <option value="choose action">--hành động--</option>
                                               <option value="Gửi mail">Gửi mail</option>
                                               <option value="Chỉnh sửa">Chỉnh sửa</option>
                                               <option value="Xóa">Xóa</option>
                                            </select>
                                         </td>
                                      </tr>
                                      <tr role="row" class="even">
                                         <th class="sorting_1">2</th>
                                         <td class="sorting_1">itsweb</td>
                                         <td>Hosting - 12gb</td>
                                         <td>
                                            <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                            <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                         </td>
                                         <td>
                                            <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa hết hạn</button><br>
                                            <i>(còn 12 ngày)</i>
                                         </td>
                                         <td>120000</td>
                                         <td>
                                            <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa thanh toán</button>
                                         </td>
                                         <td>
                                            <select class="text-center custom-select form-control" id="location1" name="location">
                                               <option value="choose action">--hành động--</option>
                                               <option value="Gửi mail">Gửi mail</option>
                                               <option value="Chỉnh sửa">Chỉnh sửa</option>
                                               <option value="Xóa">Xóa</option>
                                            </select>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                              </div> <!-- end tab-pane -->

                              <div class="tab-pane fade" id="chamsoc" role="tabpanel" aria-labelledby="chamsoc-tab">
                                  <div class="table-responsive">
                                      <table class="table table-hover mb-0"  id="tbl_chamsoc">
                                          <thead>
                                              <tr>
                                                  <th>STT</th>
                                                  <th>Nội dung chăm sóc</th>
                                                  <th>Ngày tạo</th>
                                                  <th>người tạo</th>
                                                  <th></th>
                                              </tr>
                                          </thead>
                                          <tbody class="todo-task-list-wrapper media-list">
                                              <tr >
                                                <td>1</td>
                                                <td class="todo-desc">
                                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam voluptates, fugit aut alias nisi vero praesentium.
                                                </td>
                                                <td>
                                                  01/01/2019
                                                </td>
                                                <td class="todo-title">
                                                  JohnSon
                                                </td>
                                                <td>
                                                  <button class="btn btn-default deleteChamSoc"><i class="fa fa-trash-o"></i>'</button>
                                                </td>
                                              </tr>
                                          </tbody>
                                      </table>
                                      <hr>
                                      <table class="m-1">
                                        <tr>
                                          <th>Nội dung chăm sóc (ghi chú)</th>
                                          <th>Ngày tạo</th>
                                          <th>Người tạo</th>
                                        </tr>
                                        <tr>
                                            <td class=" pr-2">
                                              <fieldset class="form-label-group">
                                                  <textarea class="form-control" id="ghichu" cols="100" rows="1" placeholder="nhập nghi chú"></textarea>
                                              </fieldset>
                                            </td>
                                            <td class=" pr-2">
                                              <fieldset class="form-label-group">
                                                <input type="date" class="form-control" id="ngaytao">
                                              </fieldset>
                                            </td>
                                            <td class=" pr-2">
                                              <fieldset class="form-label-group">
                                                  <input type="text" class="form-control" id="nguoitao">
                                              </fieldset>
                                            </td>
                                            <td class="">
                                                <button class="btn btn-outline-primary waves-effect waves-light " id="addChamSoc"  tabindex="0" aria-controls="DataTables_Table_0" style="margin-top: -25px;"><span><i class="feather icon-plus"></i>Thêm</span>
                                                </button> 
                                            </td>
                                        </tr>
                                      </table>
                                    </div>  
                              </div> <!-- end tab-pane -->

                              <div class="tab-pane fade" id="hotro" role="tabpanel" aria-labelledby="hotro-tab">
                                
                                <div id="_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                   <div class="row">
                                      <div class="col-sm-12">
                                         <table class="table zero-configuration dataTable no-footer" id="_0" role="grid" aria-describedby="_0_info">
                                            <thead>
                                               <tr role="row">
                                                  <th class="sorting_asc" >STT</th>
                                                  <th class="sorting" >MÃ</th>
                                                  <th class="sorting" >DỊCH VỤ</th>
                                                  <th class="sorting" >-NGÀY TẠO <br> -NGÀY KẾT THÚC</th>
                                                  <th class="sorting" >EMAIL</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                               <tr role="row" class="odd">
                                                  <th class="sorting_1">1</th>
                                                  <td class="">abc</td>
                                                  <td>abc</td>
                                                  <td>
                                                     <div class="d-flex">
                                                        <div class="mr-1">
                                                           <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                           <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                        </div>
                                                        <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">hoàn thành</button>
                                                     </div>
                                                  </td>
                                                  <td>abc@gmail.com</td>
                                               </tr>
                                               <tr role="row" class="even">
                                                  <th class="sorting_1">2</th>
                                                  <td class="">abc</td>
                                                  <td>abc</td>
                                                  <td>
                                                     <div class="d-flex">
                                                        <div class="mr-1">
                                                           <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                           <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                        </div>
                                                        <button type="button" class="btn btn-sm btn-warning mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">đang xử lý</button>
                                                     </div>
                                                  </td>
                                                  <td>abc@gmail.com</td>
                                               </tr>
                                            </tbody>
                                         </table>
                                      </div>
                                   </div>
                                </div>

                              </div> <!-- end tab-pane -->

                            </div>
                          </div>
                        </div> <!-- end card-content -->
                  </div>
                </div><!--  end row -->
             </div>
           </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">đóng</button>
           </div>
        </div>
     </div>
  </div>
<div class="modal-hid">
    <div class="modal fade text-left" id="viewCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" style=" padding-right: 16px;" aria-modal="true">
     <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title card-title text-center font-weight-bold with-100">Chi tiết khách hàng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
           </div>
           <div class="modal-body">
              <div class="row">
                  <div class="col-12">
                      <div class="card">
                          <div class="card-header">
                          </div>
                          <div class="card-content">
                              <div class="card-body">
                                  <div class="table-responsive">
                                    <div class="row mb-1">
                                      <div class="col-12">

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Mã khách hàng</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span for="">2345678</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Tên khách hàng</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span for="">JohnSon</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Điện thoại</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>0764567</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Email</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>abc@gmail.com</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Địa chỉ</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>Cầu Giấy - Hà Nội</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Cấp khách hàng</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>cấp doanh nghiệp</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Số lần chăm sóc</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>12</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Số lần hỗ trợ</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>12</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->


                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Công nợ</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>125678</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                        <div class="row p-1">
                                          <div class="col-5 border-right">
                                            <label for="">
                                              <strong>Ghi chú</strong>
                                            </label>
                                          </div> <!-- end col-5 -->
                                          <div class="col-7">
                                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                          </div> <!-- end col-7 -->
                                        </div> <!-- end row -->

                                      </div> <!-- end col-12 -->
                                    </div>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">đóng</button>
                          </div>
                      </div>
                  </div>
              </div><!--  end row -->
            </div><!--  end row -->
           </div>
        </div>
     </div>
</div>






<div class="modal fade" id="addNewCustomer" style="padding-right: 16px; padding-left: 16px;" aria-modal="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm khách hàng</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã khách hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên khách hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="" class="form-control" id="" placeholder="John Son">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số điện thoại</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" id="" class="form-control" name="" placeholder="012345">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Email</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="mail" id="date" class="form-control" name="date" placeholder="john@gmail.com">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Địa chỉ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" id="" class="form-control" name="" placeholder="Cầu Giấy - Hà Nội">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Cấp khách hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="">--Cấp khách hàng--</option>
                                                 <option value="">doanh nghiệp</option>
                                                 <option value="">cá nhân</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Phòng ban</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="">--Phòng ban--</option>
                                                 <option value="">abc</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Người tạo</span>
                                          </div>
                                          <div class="col-md-8">
                                            <input type="text" id="" class="form-control" name="" value="johnShep..." disabled="disabled">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-6">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-12 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Hủy</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div>

    <!-- BEGIN: TODO APP -->
    <script src="../../../app-assets/js/scripts/pages/app-todo.js"></script>
    <!-- END: TODO APP -->
    <!-- BEGIN: Page JS-->
    <script src="./app-assets/js/scripts/pages/app-todo.js"></script>
    <!-- END: Page JS-->
