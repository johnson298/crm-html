<section id="data-thumb-view" class="data-thumb-view-header">
   <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
      </div>
   </div>
   <!-- dataTable starts -->
   <div class="card">
      <div class="card-header">
         <h4 class="card-title">Sản phẩm</h4>
      </div>
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
         <div class="top p-1">
            <div class="actions action-btns d-flex justify-content-end">
               <div class="btn-group dropdown actions-dropodown">
                  <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Hành động
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-29px, 42px, 0px);">
                     <a class="dropdown-item" href="#">Xóa</a>
                     <a class="dropdown-item" href="#">In</a>
                     <a class="dropdown-item" href="#">Hành động khác</a>
                  </div>
               </div>
               <div class="dt-buttons btn-group"><button data-toggle="modal" data-target="#addNewPro" class="btn btn-outline-primary waves-effect waves-light" tabindex="0" aria-controls="DataTables_Table_0"><span><i class="feather icon-plus"></i>Tạo mới sản phẩm</span></button> </div>
            </div>
         </div>
         <div class="clear"></div>
         <div class="col-sm-12">
            <div id="_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
               <div class="row">
                  <div class="col-sm-12">
                     <table class="table zero-configuration dataTable no-footer" id="_0" role="grid" aria-describedby="_0_info">
                        <thead>
                           <tr role="row">
                              <th id="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" ></th>
                              <th class="sorting_asc" tabindex="0" aria-controls="_0" rowspan="1" colspan="1" >STT</th>
                              <th class="sorting" >TÊN DỊCH VỤ</th>
                              <th class="sorting" >LOẠI HÌNH DỊCH VỤ</th>
                              <th class="sorting" >GIÁ GỐC</th>
                              <th class="sorting" >GIÁ BÁN</th>
                              <th class="sorting" >GHI CHÚ</th>
                              <th class="sorting" >HÀNH ĐỘNG</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr role="row" class="odd">
                              <th>
                                 <input type="checkbox" name="checkAll" class="checkSingle" >
                              </th>
                              <th class="sorting_1">1</th>
                              <td class="sorting_1">hosting</td>
                              <td>cho thuê</td>
                              <td>12$</td>
                              <td>20$</td>
                              <td>abc</td>
                              <td>
                                 <div class="action-kh d-flex justify-content-around">
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPro">
                                       <i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="chỉnh sửa"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                       <i data-toggle="tooltip" title="" class="feather icon-mail" data-original-title="gửi mail"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                       <i data-toggle="tooltip" title="" class="feather icon-trash-2" data-original-title="xóa"></i>
                                       </button>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           <tr role="row" class="even">
                              <th>
                                 <input type="checkbox" name="checkAll" class="checkSingle" >
                              </th>
                              <th class="sorting_1">2</th>
                              <td class="sorting_1">website</td>
                              <td>theo yêu cầu</td>
                              <td>20$</td>
                              <td>15$</td>
                              <td>abc</td>
                              <td>
                                 <div class="action-kh d-flex justify-content-around">
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPro">
                                       <i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="chỉnh sửa"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                       <i data-toggle="tooltip" title="" class="feather icon-mail" data-original-title="gửi mail"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                       <i data-toggle="tooltip" title="" class="feather icon-trash-2" data-original-title="xóa"></i>
                                       </button>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           <tr role="row" class="even">
                              <th>
                                 <input type="checkbox" name="checkAll" class="checkSingle" >
                              </th>
                              <th class="sorting_1">2</th>
                              <td class="sorting_1">website</td>
                              <td>theo yêu cầu</td>
                              <td>20$</td>
                              <td>15$</td>
                              <td>abc</td>
                              <td>
                                 <div class="action-kh d-flex justify-content-around">
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPro">
                                       <i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="chỉnh sửa"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                       <i data-toggle="tooltip" title="" class="feather icon-mail" data-original-title="gửi mail"></i>
                                       </button>
                                    </div>
                                    <div class="fonticon-wrap">
                                       <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                       <i data-toggle="tooltip" title="" class="feather icon-trash-2" data-original-title="xóa"></i>
                                       </button>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- dataTable ends -->
   <div class="modal-hid">
      <div class="modal fade" id="addNewPro">
         <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm sản phẩm</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control" name="contact" placeholder="nhập tên dịch vụ" required="required">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Loại hình dịch vụ</span>
                                          </div>
                                          <div class="col-md-8 d-flex justify-content-around pl-0">
                                             <fieldset>
                                                <div class="vs-radio-con">
                                                   <input type="radio" name="loaihinh" checked="" value="Cho thuê">
                                                   <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                                   </span>
                                                   <span class="">Cho thuê</span>
                                                </div>
                                             </fieldset>
                                             <fieldset>
                                                <div class="vs-radio-con">
                                                   <input type="radio" name="loaihinh" checked="" value="Theo yêu cầu">
                                                   <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                                   </span>
                                                   <span class="">Theo yêu cầu</span>
                                                </div>
                                             </fieldset>
                                             <fieldset>
                                                <div class="vs-radio-con">
                                                   <input type="radio" name="loaihinh" checked="" value="Chăm sóc theo tháng">
                                                   <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                                   </span>
                                                   <span class="">Chăm sóc theo tháng</span>
                                                </div>
                                             </fieldset>
                                             <input type="hidden" id="loaihinh" value="Chăm sóc theo tháng">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" id="takeCareOf" style="display: none;">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Cho thuê theo ngày</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Cho thuê theo tháng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Cho thuê theo năm</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" id="quantityMonth" style="">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Dịch vụ: </span>
                                             <input type="text" id="nameService" class="form-control" placeholder="dịch vụ">
                                          </div>
                                          <div class="col-md-8">
                                             <span>Số lượng:</span>
                                             <span><input type="number" id="quantitySer" class="form-control" name="contact" placeholder="Nhập số lượng"></span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" id="priceRent" style="display: none;">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Giá gốc </span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control" placeholder="nhập giá gốc">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Giá bán </span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control" placeholder="nhập giá bán">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" id="quantityMonth">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú: </span>
                                          </div>
                                          <div class="col-md-8">
                                             <textarea class="form-control" id="notePro" rows="3" placeholder="Nhập ghi chú"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" id="tbl_infoPro" style="">
                                       <div class="table-responsive">
                                          <div class="col-12 d-flex justify-content-end pr-0">
                                             <button id="newPro" class="btn btn-success btn-sm mb-1 waves-effect waves-light">thêm công việc <span>+</span></button>
                                          </div>
                                          <table class="table dataTable no-footer" id="tbl_infoPro_show">
                                             <thead>
                                                <tr>
                                                   <th class="">DỊCH VỤ</th>
                                                   <th class="">lOẠI HÌNH DỊCH VỤ</th>
                                                   <th class="">SỐ LƯỢNG</th>
                                                   <th class="">GHI CHÚ</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                             </tbody>
                                          </table>
                                       </div>
                                       <!-- END TABLE-RESPONSIVE -->
                                    </div>
                                    <div class="col-md-8 mt-2 offset-md-4 d-flex justify-content-end pr-0">
                                       <button id="" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm mới</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                        <!-- end card-body -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="editPro" style="padding-right: 16px;" aria-modal="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Chỉnh sửa sản phẩm</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Loại hình dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                <option value="">cho thuê</option>
                                                <option value="">theo yêu cầu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Giá gốc</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="12" class="form-control" id="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Giá bán</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="12" class="form-control" id="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                                <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                             </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Lưu</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                        <!-- end card-body -->
                     </div>
                  </div>
                  <!-- end card -->
               </div>
               <!-- end modal body -->
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
      </div>

   </div>
</section>