<div class="content-header row">
   <div class="content-header-left col-md-9 col-12 mb-2">
      <div class="row breadcrumbs-top">
         <div class="col-12">
            <h2 class="content-header-title float-left mb-0">DataTables</h2>
            <div class="breadcrumb-wrapper col-12">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Datatable
                  </li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
      <div class="form-group breadcrum-right">
         <div class="dropdown">
            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
         </div>
      </div>
   </div>
</div>
<!-- END CONTENT-HEADER ROW -->
<div class="content-body">
   <!-- Zero configuration table -->
   <section id="basic-datatable">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <h4 class="card-title">Đơn hàng</h4>
               </div>
               <div class="card-content">
                  <div class="card-body card-dashboard">
                     <p class="card-text">DataTables has most features enabled by default, so all you need to do to use it with your own ables is to call the construction function: $().DataTable();.</p>
                     <div class="table-responsive">
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade show active" id="dongia" role="tabpanel" aria-labelledby="home-tab">
                              <div id="" class="dataTables_wrapper dt-bootstrap4">
                                 <div class="row">
                                  <div class="top pt-2 pb-2 col-12">
                                    <div class="actions action-btns d-flex justify-content-end">
                                       <div class="dt-buttons btn-group"><button data-toggle="modal" data-target="#addOrder" class="btn btn-outline-primary waves-effect waves-light" tabindex="0" aria-controls="DataTables_Table_0"><span><i class="feather icon-plus"></i>Thêm dịch vụ</span></button> </div>
                                    </div>
                                  </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <table class="table zero-configuration dataTable" id="_0" role="grid" aria-describedby="_0_info">
                                          <thead>
                                             <tr role="row">
                                                <th id="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" ></th>
                                                <th class="sorting_asc">STT</th>
                                                <th class="sorting">MÃ ĐƠN HÀNG</th>
                                                <th class="sorting">TÊN KHÁCH HÀNG</th>
                                                <th class="sorting">NGÀY TẠO ĐƠN</th>
                                                <th class="sorting">TT DỊCH VỤ</th>
                                                <th class="sorting">TT THANH TOÁN</th>
                                                <th class="sorting">TÔNG TIỀN</th>
                                                <th class="sorting">NGƯỜI TẠO ĐƠN</th>
                                                <th class="sorting">HÀNH ĐỘNG</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr role="row" class="odd">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
                                                <th>1</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   01/01/2019
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">chưa hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">đã thanh toán</button>
                                                </td>
                                                <td>120000</td>
                                                <td>abcde</td>
                                                <td>
                                                  <div class="d-flex justify-content-around">
                                                    <div class="fonticon-wrap text-center">
                                                      <a href="don_hang.php" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil text-dark"></i>
                                                      </a>
                                                     </div>
                                                    <div class="fonticon-wrap text-center">
                                                      <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2 text-dark"></i>
                                                      </a>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="even">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
                                                <th>2</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   01/01/2019
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa thanh toán</button>
                                                </td>
                                                <td>120000</td>
                                                <td>abcde</td>
                                                <td>
                                                  <div class="d-flex justify-content-around">
                                                    <div class="fonticon-wrap text-center">
                                                      <a href="don_hang.php" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil text-dark"></i>
                                                      </a>
                                                     </div>
                                                    <div class="fonticon-wrap text-center">
                                                      <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2 text-dark"></i>
                                                      </a>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="odd">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle" ></th>
                                                <th>3</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   01/01/2019
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6">đã hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-warning mr-1 mb-1 waves-effect waves-light p-cus-6">thanh toán còn thiếu</button>
                                                </td>
                                                <td>120000</td>
                                                <td>abcde</td>
                                                <td>
                                                  <div class="d-flex justify-content-around">
                                                    <div class="fonticon-wrap text-center">
                                                      <a href="don_hang.php" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil text-dark"></i>
                                                      </a>
                                                     </div>
                                                    <div class="fonticon-wrap text-center">
                                                      <a data-toggle="modal" data-target="#warning-delete" href="javascript:void(0)" class="border-0 background-0 cursor-pointer">
                                                         <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2 text-dark"></i>
                                                      </a>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                             
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- end row -->
                              </div>
                           </div> <!-- end tab-pan -->

                        </div>  
                     </div>
                  </div> <!-- end card-body -->
               </div> <!-- end card-content -->
            </div> <!-- end card -->
         </div> <!-- end col -->
      </div> <!-- end row -->
   </section>
   <!--/ Zero configuration table -->
</div>


   <div class="modal-hid">
      <div class="modal fade" id="addOrder">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm đơn hàng</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Loại hình dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="">cho thuê</option>
                                                 <option value="">bán</option>
                                                 <option value=""></option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn số tháng thuê</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="contact-info" class="form-control" name="contact" placeholder="chọn số tháng">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn ngày đăng ký</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="date" id="date" class="form-control" name="date" placeholder="chọn ngày đăng ký">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số tiền</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="number" class="form-control" name="number" placeholder="chọn số tiền">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">Đã thanh toán</option>
                                                 <option value="Chỉnh sửa">Chưa thanh toán</option>
                                                 <option value="Xóa">Thanh toán còn thiếu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="modal fade" id="addPays">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm thanh toán</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">itsweb</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số tiền thanh toán</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="120000">120000</option>
                                                 <option value="120000">120000</option>
                                                 <option value="120000">120000</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">Đã thanh toán</option>
                                                 <option value="Chỉnh sửa">Chưa thanh toán</option>
                                                 <option value="Xóa">Thanh toán còn thiếu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div>
   <!-- BEGIN: Vendor JS-->
   <script src="./app-assets/vendors/js/vendors.min.js"></script>
   <!-- BEGIN Vendor JS-->

   <!-- BEGIN: Page Vendor JS-->
   <script src="./app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
   <script src="./app-assets/js/core/app-menu.js"></script>
   <script src="./app-assets/js/core/app.js"></script>
   <script src="./app-assets/js/scripts/components.js"></script>
   <!-- END: Theme JS-->

   <!-- BEGIN: Page JS-->
   <script src="./app-assets/js/scripts/forms/select/form-select2.js"></script>
   <!-- END: Page JS-->