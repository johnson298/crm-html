<div class="title-view">
  <h3 class="bg-info p-1 border rounded text-center text-white mb-2">Danh sách phòng ban</h3>
</div>
<ul id="myUL">
  <li><span class="caret caret-down">HOBASOFT</span>
    <ul class="nested active">
      <li class="cursor-pointer">Ban giám đốc</li>
      <li class="cursor-pointer">Phòng kinh doanh HCM</li>
      <li class="cursor-pointer"><span class="caret caret-down">Phòng kinh doanh HN</span>
        <ul class="nested active">
          <li class="cursor-pointer">Trưởng phòng</li>
          <li class="cursor-pointer">Nhân sự</li>
        </ul>
      </li>  
    </ul>
  </li>
</ul>