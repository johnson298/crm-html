<div class="card min-height-3">
   <div class="card-header border-bottom p-1">
      <h4 class="card-title">Thêm phòng ban</h4>
   </div>
   <div class="card-content mt-2">
      <div class="row m-2">
         <div class="col-6 p-0">
            <div class="col-12">
               <div class="row p-1">
                  <div class="col-5 text-right">
                     <label for="">
                     <strong>Tên phòng ban</strong>
                     </label>
                  </div>
                  <!-- end col-5 -->
                  <div class="col-7">
                     <input type="text" class="form-control" placeholder="nhập tên phòng ban" value="">
                  </div>
                  <!-- end col-7 -->
               </div>
               <!-- end row -->
               <div class="row p-1">
                  <div class="col-5 text-right">
                     <label for="">
                     <strong>Mã phòng ban</strong>
                     </label>
                  </div>
                  <!-- end col-5 -->
                  <div class="col-7">
                     <input type="text" class="form-control" placeholder="" value="nhập mã phòng ban">
                  </div>
                  <!-- end col-7 -->
               </div>
               <!-- end row -->
               <div class="row p-1">
                  <div class="col-5 text-right">
                     <label for="">
                     <strong>Đơn vị cha</strong>
                     </label>
                  </div>
                  <!-- end col-5 -->
                  <div class="col-7">
                     <select class="form-control" id="basicSelect">
                        <option>--Lựa chọn--</option>
                        <option>Ban giám đốc</option>
                        <option>Phòng kinh doanh HCM</option>
                        <option>Phòng kinh doanh Hà Nội</option>
                     </select>
                  </div>
                  <!-- end col-7 -->
               </div>
               <!-- end row -->
               <div class="row p-1">
                  <div class="col-5 text-right">
                     <label for="">
                     <strong>Trưởng đơn vị</strong>
                     </label>
                  </div>
                  <!-- end col-5 -->
                  <div class="col-7">
                     <select class="form-control" id="basicSelect">
                        <option>--Lựa chọn--</option>
                        <option>Adminstrator</option>
                        <option>Lại Thế Sơn</option>
                        <option>Nguyễn Văn A</option>
                     </select>
                  </div>
                  <!-- end col-7 -->
               </div>
               <!-- end row -->
            </div>
         </div>
         <!-- end col-6 -->
         <div class="col-6 p-0">
            <div class="row p-1">
               <div class="col-5 text-right">
                  <span><strong>Ngày tạo</strong></span>
               </div>
               <div class="col-7">
                  <input type="text" class="form-control" value="20/10/2019" disabled="">
               </div>
            </div>
            <!-- end row -->
            <div class="row p-1">
               <div class="col-5 text-right">
                  <label for="">
                  <strong>Người tạo</strong>
                  </label>
               </div>
               <!-- end col-5 -->
               <div class="col-7">
                  <input type="text" class="form-control" value="Adminstrator" disabled="">
               </div>
               <!-- end col-7 -->
            </div>
            <!-- end row -->
         </div>
         <!-- end col-6 -->
         <div class="col-12 m-1 border-top text-right">
            <div class="mt-2">
               <button class="btn btn-info">Lưu</button>
               <button class="btn btn-danger">Hủy</button>
            </div>
         </div>				
      </div>
   </div>
   <!-- card-content -->
</div>
<script src="./app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="./app-assets/js/scripts/forms/select/form-select2.js"></script>