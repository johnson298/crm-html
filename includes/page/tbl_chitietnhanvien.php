<div class="col-12">
	<h6 class="p-1 border-bottom">Thông tin tài khoản</h6>
</div>
<div class="col-6 p-0">
	<div class="col-12">
		<div class="row p-1">
			<div class="col-5 text-right">
				<label for="">
					<strong>Tên đăng nhập</strong>
				</label>
			</div>
			<!-- end col-5 -->
			<div class="col-7">
				<input type="text" class="form-control" placeholder="tên đăng nhập" value="admin" disabled="">
			</div>
			<!-- end col-7 -->
		</div>
		<!-- end row -->
		<div class="row p-1">
			<div class="col-5 text-right">
				<label for="">
					<strong>Mật khẩu</strong>
				</label>
			</div>
			<!-- end col-5 -->
			<div class="col-7">
				<button class="btn btn-light text-dark">Đổi mật khẩu</button>
			</div>
			<!-- end col-7 -->
		</div>
		<!-- end row -->
		<div class="row p-1">
			<div class="col-5 text-right">
				<label for="">
					<strong>Họ tên</strong>
				</label>
			</div>
			<!-- end col-5 -->
			<div class="col-7">
				<input type="text" class="form-control" placeholder="" value="adminstrator">
			</div>
			<!-- end col-7 -->
		</div>
		<!-- end row -->
		<div class="row p-1">
			<div class="col-5 text-right">
				<label for="">
					<strong>Đơn vị</strong>
				</label>
			</div>
			<!-- end col-5 -->
			<div class="col-7">
				<div class="form-group mb-0">
					<select class="select2 form-control select2-hidden-accessible" multiple="" data-select2-id="12" tabindex="-1" aria-hidden="true">
						<option value="bán giám đốc" data-select2-id="52">bán giám đốc</option>
						<option value="nhân viên" selected="" data-select2-id="62">nhân viên</option>
						<option value="ban nhân viên" data-select2-id="72">ban nhân viên</option>
					</select>
					<span class="select2 select2-container select2-container--default select2-container--focus select2-container--below" dir="ltr" data-select2-id="56" style="width: 563.8px;">
						<span class="selection">
							<span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
								<ul class="select2-selection__rendered">
									<li class="select2-selection__choice" title="Triangle" data-select2-id="52"><span class="select2-selection__choice__remove" role="presentation">×</span>ban giám đốc</li>
									<li class="select2-selection__choice" title="ban nhân viên" data-select2-id="72"><span class="select2-selection__choice__remove" role="presentation">×</span>ban nhân viên</li>
								</ul>
							</span>
						</span>
						<span class="dropdown-wrapper" aria-hidden="true"></span>
					</span>
				</div>
			</div>
			<!-- end col-7 -->
		</div>
		<!-- end row -->
	</div>
</div> <!-- end col-6 -->

<div class="col-6 p-0">
	<div class="row p-1">
		<div class="col-5 text-right">
			<span>Ảnh đại diện</span>
		</div>
		<div class="col-7">
			<div class="custom-file">
                <input type="file" class="custom-file-input" id="inputGroupFile01">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
		</div>
	</div> <!-- end row -->

	<div class="row p-1">
		<div class="col-5 text-right">
			<label for="">
				<strong>Vai trò</strong>
			</label>
		</div>
		<!-- end col-5 -->
		<div class="col-7">
			<select class="form-control" id="basicSelect">
				<option>lãnh đạo</option>
				<option>nhân viên</option>
			</select>
		</div>
		<!-- end col-7 -->
	</div>
	<!-- end row -->
	<div class="row p-1">
		<div class="col-5 text-right">
			<label for="">
				<strong>Nhóm quyền</strong>
			</label>
		</div>
		<!-- end col-5 -->
		<div class="col-7">
			<div class="form-group mb-0" style="height: 40px;">
				<select class="select2 form-control select2-hidden-accessible" multiple="multiple" data-select2-id="145" tabindex="-1" aria-hidden="true">
					<option value="admin" data-select2-id="1">admin</option>
					<option value="nhân viên" selected="" data-select2-id="2">nhân viên</option>
				</select>
				<span class="select2 select2-container select2-container--default select2-container--focus select2-container--below" dir="ltr" data-select2-id="121" style="width: 563.8px;">
					<span class="selection">
						<span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
							<ul class="select2-selection__rendered">
								<li class="select2-selection__choice" title="admin" data-select2-id="1"><span class="select2-selection__choice__remove" role="presentation">×</span>admin</li>
							</ul>
						</span>
					</span>
					<span class="dropdown-wrapper" aria-hidden="true"></span>
				</span>
			</div>
		</div>
		<!-- end col-7 -->
	</div>
		<!-- end row -->
	<div class="row p-1">
		<div class="col-5 text-right">
			<label for=""><strong>Hiệu lực từ</strong></label>
		</div>
		<div class="col-7 d-flex align-items-center">
			<input type="date" class="form-control">
			<span class="mr-"><strong>-</strong></span>
			<input type="date" class="form-control">
		</div>
	</div> <!-- end row -->
</div> <!-- end col-6 -->

<div class="col-12">
	<h6 class="p-1 border-bottom">Thông tin địa chỉ</h6>
</div> <!-- end col-12 -->

<div class="col-6">
	<div class="row p-1">
		<div class="col-5 text-right">
			<label for="">
				<strong>Điện thoại di động</strong>
			</label>
		</div>
		<!-- end col-5 -->
		<div class="col-7">
			<input type="text" class="form-control" placeholder="nhập số điện thoại" value="098765432">
		</div>
		<!-- end col-7 -->
	</div>
	<!-- end row -->
	<div class="row p-1">
		<div class="col-5 text-right">
			<label for="">
				<strong>Email</strong>
			</label>
		</div>
		<!-- end col-5 -->
		<div class="col-7">
			<input class="form-control" type="mail" placeholder="nhập email" value="abc@gmail.com">
		</div>
		<!-- end col-7 -->
	</div>
	<!-- end row -->
</div> <!-- end col-6 -->

<div class="col-12 m-1">
	<div class="d-flex justify-content-end">
		<button class="btn btn-info mr-2">Lưu</button><button class="btn btn-danger">Hủy</button>
	</div>
</div>
<script src="./app-assets/vendors/js/forms/select/select2.full.min.js"></script>

<script src="./app-assets/js/scripts/forms/select/form-select2.js"></script>