<div class="content-header row">
   <div class="content-header-left col-md-9 col-12 mb-2">
      <div class="row breadcrumbs-top">
         <div class="col-12">
            <h2 class="content-header-title float-left mb-0">DataTables</h2>
            <div class="breadcrumb-wrapper col-12">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Datatable
                  </li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
      <div class="form-group breadcrum-right">
         <div class="dropdown">
            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
         </div>
      </div>
   </div>
</div>
<!-- END CONTENT-HEADER ROW -->
<div class="content-body">
   <!-- Zero configuration table -->
   <section id="basic-datatable">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <h4 class="card-title">Đơn giá</h4>
               </div>
               <div class="card-content">
                  <div class="card-body card-dashboard">
                     <div class="table-responsive">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                             <a class="nav-link active" id="tongquan-tab" data-toggle="tab" href="#tongquan" role="tab" aria-controls="tongquan" aria-selected="false">TỔNG QUAN</a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="dongia-tab" data-toggle="tab" href="#dongia" role="tab" aria-controls="dongia" aria-selected="true">DỊCH VỤ</a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="thanhtoan-tab" data-toggle="tab" href="#thanhtoan" role="tab" aria-controls="thanhtoan" aria-selected="false">THANH TOÁN</a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="hotro-tab" data-toggle="tab" href="#hotro" role="tab" aria-controls="hotro" aria-selected="false">HỖ TRỢ</a>
                          </li>
                          <li class="nav-item">
                             <a class="nav-link" id="congviec-tab" data-toggle="tab" href="#congviec" role="tab" aria-controls="congviec" aria-selected="false">CÔNG VIỆC</a>
                          </li>
                       </ul>
                        <div class="tab-content" id="myTabContent">

                           <div class="tab-pane fade show active" id="tongquan" role="tabpanel" aria-labelledby="profile-tab">
                              <div id="" class="dataTables_wrapper dt-bootstrap4">
                                  <div class="row mb-1">
                                        <div class="col-6 p-1">
                                          <div class="row">
                                             <div class="col-5">
                                                <label for="">
                                                <strong>Tên khách hàng</strong>
                                                </label>
                                             </div>
                                             <!-- end col-5 -->
                                             <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập tên khách hàng" value="abc">
                                             </div>
                                             <!-- end col-7 -->
                                          </div>
                                        </div>
                                        <!-- end row -->

                                        <div class="col-6 p-1">
                                          <div class="row">
                                             <div class="col-5">
                                                <label for="">
                                                <strong>Địa chỉ</strong>
                                                </label>
                                             </div>
                                             <!-- end col-5 -->
                                             <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập tên địa chỉ" value="Cầu Giấy - Hà Nội">
                                             </div>
                                             <!-- end col-7 -->
                                          </div>
                                        </div>
                                        <!-- end row -->

                                        <div class="col-6 p-1">
                                          <div class="row">
                                             <div class="col-5">
                                                <label for="">
                                                <strong>Điện thoại</strong>
                                                </label>
                                             </div>
                                             <!-- end col-5 -->
                                             <div class="col-7">
                                                <input type="number" class="form-control" placeholder="nhập số điện thoại" value="0912837">
                                             </div>
                                             <!-- end col-7 -->
                                         </div>
                                        </div>
                                        <!-- end row -->

                                        <div class="col-6 p-1">
                                          <div class="row">
                                             <div class="col-5">
                                                <label for="">
                                                <strong>Email</strong>
                                                </label>
                                             </div>
                                             <!-- end col-5 -->
                                             <div class="col-7">
                                                <input type="text" class="form-control" placeholder="nhập email" value="abc@gmail.com">
                                             </div>
                                             <!-- end col-7 -->
                                         </div>
                                        </div>
                                        <!-- end row -->

                                        <div class="col-6 p-1">
                                          <div class="row">
                                             <div class="col-5">
                                                <label for="">
                                                <strong>Loại khách hàng</strong>
                                                </label>
                                             </div>
                                             <!-- end col-5 -->
                                             <div class="col-7">
                                                <select name="" id="" class="text-center custom-select form-control">
                                                  <option value="">cá nhân</option>
                                                  <option value="">doanh nghiệp</option>
                                                </select>
                                             </div>
                                             <!-- end col-7 -->
                                         </div>
                                        </div>
                                        <!-- end row -->

                                  </div>                                
                              </div>
                           </div> <!-- end tab-pan -->

                           <div class="tab-pane fade" id="dongia" role="tabpanel" aria-labelledby="home-tab">
                              <div id="" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                  <div class="top pt-2 pb-2 col-12">
                                    <div class="actions action-btns d-flex justify-content-end">
                                       <div class="dt-buttons btn-group"><button data-toggle="modal" data-target="#addOrder" class="btn btn-outline-primary waves-effect waves-light" tabindex="0" aria-controls="DataTables_Table_0"><span><i class="feather icon-plus"></i>Tạo mới đơn hàng</span></button> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                       <table class="table zero-configuration dataTable" id="_0" role="grid" aria-describedby="_0_info">
                                          <thead>
                                             <tr role="row">
                                                <th id="remove-sorting" class="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" class="checkedAll_class"></th>
                                                <th class="sorting_asc" >STT</th>
                                                <th class="sorting" >MÃ ĐƠN HÀNG</th>
                                                <th class="sorting" >DỊCH VỤ</th>
                                                <th class="sorting" >-NGÀY ĐĂNG KÝ<br/>- NGÀY HẾT HẠN</th>
                                                <th class="sorting" >TT DỊCH VỤ</th>
                                                <th class="sorting" >GIẢM GIÁ</th>
                                                <th class="sorting" >TÔNG TIỀN</th>
                                                <th class="sorting" >TT THANH TOÁN</th>
                                                <th class="sorting" >HÀNH ĐỘNG</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr role="row" class="odd">
                                              <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>1</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                   <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">chưa hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>2%</td>
                                                <td>120000</td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">đã thanh toán</button>
                                                </td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editOrder">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="even">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>2</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                   <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>2%</td>
                                                <td>120000</td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6">Chưa thanh toán</button>
                                                </td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editOrder">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="odd">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>3</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>Hosting - 12gb</td>
                                                <td>
                                                   <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                   <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6">đã hết hạn</button><br>
                                                   <i>(còn 12 ngày)</i>
                                                </td>
                                                <td>2%</td>
                                                <td>120000</td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-warning mr-1 mb-1 waves-effect waves-light p-cus-6">thanh toán còn thiếu</button>
                                                </td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editOrder">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                </div> <!-- end row -->
                                <div class="row mt-1">
                                  <div class="col-md-3"></div>
                                  <div class="col-md-3"></div>
                                  <div class="col-md-3"></div>
                                  <div class="col-md-3">
                                    <div class="total-dongia">
                                      <div class="row">
                                        <div class="col-md-6 p-1 border-bottom"><strong>Tổng tiền</strong></div>
                                        <div class="col-md-6 p-1 border-bottom"><strong>999999</strong></div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6 p-1 border-bottom"><strong>Thuế VAT</strong></div>
                                        <div class="col-md-6 p-1 border-bottom"><strong>999999</strong></div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6 p-1 border-bottom"><strong>Tổng tiền thanh toán</strong></div>
                                        <div class="col-md-6 p-1 border-bottom"><strong>999999</strong></div>
                                      </div>
                                    </div>
                                  </div>
                                </div> <!-- end row -->
                                
                              </div>
                           </div> <!-- end tab-pan -->

                           <div class="tab-pane fade" id="thanhtoan" role="tabpanel" aria-labelledby="contact-tab">
                              <div id="" class="dataTables_wrapper dt-bootstrap4">
                                 <div class="row">
                                     <div class="col"></div>
                                     <div class="col"></div>
                                     <div class="col"></div>
                                     <div class="col pr-0 d-flex flex-end justify-content-end">
                                        <button type="button" class="btn btn-outline-primary waves-effect waves-light mr-1 waves-effect waves-light p-1 mb-0" data-toggle="modal" data-target="#addPays">thêm thanh toán</button>
                                     </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <table class="table zero-configuration dataTable" id="_0" role="grid" aria-describedby="_0_info">
                                          <thead>
                                             <tr role="row">
                                                <th id="remove-sorting" class="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" class="checkedAll_class"></th>
                                                <th class="sorting" >STT</th>
                                                <th class="sorting" >TÊN DỊCH VỤ</th>
                                                <th class="sorting" >SỐ TIỀN</th>
                                                <th class="sorting" >NGÀY THANH TOÁN</th>
                                                <th class="sorting" >HÌNH THỨC THANH TOÁN</th>
                                                <th class="sorting" >NGƯỜI TẠO ĐƠN</th>
                                                <th class="sorting" >TRẠNG THÁI</th>
                                                <th class="sorting" >HÀNH ĐỘNG</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr role="row" class="odd">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>1</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>120000</td>
                                                <td>- 01/01/2019</p></td>
                                                <td>Tiền mặt</td>
                                                <td></td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light">hủy</button>
                                                </td>
                                                <td>
                                                   <div class="action-kh d-flex justify-content-around">
                                                      <div class="fonticon-wrap">
                                                         <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPay">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                         </button>
                                                      </div>
                                                      <div class="fonticon-wrap">
                                                         <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="feather icon-mail"></i>
                                                         </button>
                                                      </div>
                                                      <div class="fonticon-wrap">
                                                         <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="feather icon-trash-2"></i>
                                                         </button>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="even">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>2</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>120000</td>
                                                <td>- 01/01/2019</p></td>
                                                <td>Tiền mặt</td>
                                                <td></td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light">xét duyệt</button>
                                                </td>
                                                <td>
                                                   <div class="action-kh d-flex justify-content-around">
                                                      <div class="fonticon-wrap">
                                                         <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPay">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                         </button>
                                                      </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr role="row" class="odd">
                                                <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                                <th>3</th>
                                                <td class="sorting_1">itsweb</td>
                                                <td>120000</td>
                                                <td>- 01/01/2019</p></td>
                                                <td>Ngân hàng</td>
                                                <td></td>
                                                <td>
                                                   <button type="button" class="btn btn-sm btn-warning mr-1 mb-1 waves-effect waves-light">chưa xét duyệt</button>
                                                </td>
                                                <td>
                                                   <div class="action-kh d-flex justify-content-around">
                                                      <div class="fonticon-wrap">
                                                         <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editPay">
                                                         <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                         </button>
                                                      </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- end row -->
                              </div>
                           </div> <!-- end tab-pan -->

                           <div class="tab-pane fade" id="hotro" role="tabpanel" aria-labelledby="profile-tab">
                              <div id="" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                   <div class="top pt-2 pb-2 col-12">
                                      <div class="actions action-btns d-flex justify-content-end">
                                       <div class="dt-buttons btn-group">
                                        <button data-toggle="modal" data-target="#addSupport" class="btn btn-outline-primary waves-effect waves-light" tabindex="0" aria-controls="DataTables_Table_0"><span><i class="feather icon-plus"></i>Thêm hỗ trợ</span></button> 
                                      </div>
                                    </div>
                                   </div>
                                </div>
                               <div class="row">
                                  <div class="col-sm-12">
                                     <table class="table zero-configuration dataTable no-footer" id="_0" role="grid" aria-describedby="_0_info">
                                        <thead>
                                           <tr role="row">
                                              <th id="remove-sorting" class="remove-sorting"><input type="checkbox" name="checkedAll" id="checkedAll" class="checkedAll_class"></th>
                                              <th class="sorting" >STT</th>
                                              <th class="sorting" >MÃ</th>
                                              <th class="sorting" >DỊCH VỤ</th>
                                              <th class="sorting_desc" >-NGÀY TẠO <br> -NGÀY KẾT THÚC</th>
                                              <th class="sorting" >EMAIL</th>
                                              <th class="sorting" >HÀNH ĐỘNG</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                           <tr role="row" class="odd">
                                              <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                              <th class="">1</th>
                                              <td class="">abc</td>
                                              <td>abc</td>
                                              <td>
                                                <div class="d-flex">
                                                  <div class="mr-1">
                                                     <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                     <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                  </div>
                                                  <button type="button" class="btn btn-sm btn-success mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">hoàn thành</button>
                                                </div>
                                              </td>
                                              <td>abc@gmail.com</td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editSupport">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                           </tr>
                                           <tr role="row" class="even">
                                              <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                              <th class="">2</th>
                                              <td class="">abc</td>
                                              <td>abc</td>
                                              <td>
                                                <div class="d-flex">
                                                  <div class="mr-1">
                                                     <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                     <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                  </div>
                                                  <button type="button" class="btn btn-sm btn-warning mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">đang xử lý</button>
                                                </div>
                                              </td>
                                              <td>abc@gmail.com</td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editSupport">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                           </tr>
                                           <tr role="row" class="odd">
                                              <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                              <th class="">3</th>
                                              <td class="">abc</td>
                                              <td>abc</td>
                                              <td>
                                                <div class="d-flex">
                                                  <div class="mr-1">
                                                     <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                     <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                  </div>
                                                  <button type="button" class="btn btn-sm btn-primary mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">đang chờ</button>
                                                </div>
                                              </td>
                                              <td>abc@gmail.com</td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editSupport">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                           </tr>
                                           </tr>
                                           <tr role="row" class="odd">
                                              <th><input type="checkbox" name="checkAll" class="checkSingle"></th>
                                              <th class="">4</th>
                                              <td class="">abc</td>
                                              <td>abc</td>
                                              <td>
                                                <div class="d-flex">
                                                  <div class="mr-1">
                                                     <p class="m-0 table-p day-regis">- 01/01/2019</p>
                                                     <p class="m-0 table-p day-expire">- 01/01/2019</p>
                                                  </div>
                                                  <button type="button" class="btn btn-sm btn-danger mr-1 mb-1 waves-effect waves-light p-cus-6 mt-1">hủy</button>
                                                </div>
                                              </td>
                                              <td>abc@gmail.com</td>
                                                <td>
                                                  <div class="action-kh d-flex justify-content-around">
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#editSupport">
                                                        <i data-toggle="tooltip" title="chỉnh sửa" class="fa fa-pencil"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#">
                                                        <i data-toggle="tooltip" title="gửi mail" class="feather icon-mail"></i>
                                                        </button>
                                                     </div>
                                                     <div class="fonticon-wrap">
                                                        <button class="border-0 background-0 cursor-pointer" data-toggle="modal" data-target="#warning-delete">
                                                        <i data-toggle="tooltip" title="xóa" class="feather icon-trash-2"></i>
                                                        </button>
                                                     </div>
                                                  </div>
                                                </td>
                                           </tr>
                                        </tbody>
                                     </table>
                                  </div>
                               </div>
                              </div> <!-- end row -->
                           </div> <!-- end tab-pan -->

                           <div class="tab-pane fade" id="congviec" role="tabpanel" aria-labelledby="profile-tab">
                              <div id="_0_wrapper" class=" dt-bootstrap4 no-footer">
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <table class="table dataTable no-footer" id="_0" role="grid" aria-describedby="_0_info">
                                          <thead>
                                             <tr role="row">
                                                <th class="sorting" >TÊN CÔNG VIỆC</th>
                                                <th class="sorting" >CÔNG VIỆC ĐÃ LÀM</th>
                                                <th class="sorting" >SỐ LƯỢNG</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr role="row" class="odd">
                                                <td class="">update nội dung</td>
                                                <td>
                                                  <input type="text" class="form-control" value="2">
                                                </td>
                                                <td>2</td>
                                             </tr>
                                             <tr role="row" class="even">
                                                <td class="">bài viết</td>
                                                <td>  
                                                  <input type="text" class="form-control" value="2">
                                                </td>
                                                <td>3</td>
                                             </tr>
                                             <tr role="row" class="even">
                                                <td class="">Thiết kế banner</td>
                                                <td>
                                                  <input type="text" class="form-control" value="2">
                                                </td>
                                                <td>4</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end mt-1">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Lưu</button>
                                       <button type="button" class="btn btn-primary waves-effect waves-light mb-1" data-dismiss="modal">Lưu và gửi mail</button>
                                    </div>
                                 </div>
                              </div>
                           </div> <!-- end tab-pan -->

                        </div>  
                     </div>
                  </div> <!-- end card-body -->
               </div> <!-- end card-content -->
            </div> <!-- end card -->
         </div> <!-- end col -->
      </div> <!-- end row -->
   </section>
   <!--/ Zero configuration table -->
</div>


   <div class="modal-hid">
      <div class="modal fade" id="addOrder">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm đơn hàng</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Loại hình dịch vụ</span>
                                          </div>
                                          <div class="col-md-8" data-select2-id="9">
                                             <div class="">
                                               <div class="form-group mb-0" data-select2-id="8">
                                                  <select class="select2 form-control select2-hidden-accessible" multiple="" data-select2-id="12" tabindex="-1" aria-hidden="true">
                                                     <option value="cho thuê" data-select2-id="52">cho thuê</option>
                                                     <option value="bán" selected="" data-select2-id="62">bán</option>
                                                  </select>
                                                  <span class="select2 select2-container select2-container--default select2-container--below select2-container--focus" dir="ltr" data-select2-id="1" style="width: 1px;">
                                                     <span class="selection">
                                                        <span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                                           <ul class="select2-selection__rendered">
                                                              <li class="select2-selection__choice" title="bán" data-select2-id="28"><span class="select2-selection__choice__remove" role="presentation">×</span>bán</li>
                                                              <li class="select2-selection__choice" title="cho thuê" data-select2-id="29"><span class="select2-selection__choice__remove" role="presentation">×</span>cho thuê</li>
                                                              <li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>
                                                           </ul>
                                                        </span>
                                                     </span>
                                                  </span>
                                               </div>
                                            </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn số tháng thuê</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="contact-info" class="form-control" name="contact" placeholder="chọn số tháng">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn ngày đăng ký</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="date" id="date" class="form-control" name="date" placeholder="chọn ngày đăng ký">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số tiền</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="number" class="form-control" name="number" placeholder="chọn số tiền">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">Đã thanh toán</option>
                                                 <option value="Chỉnh sửa">Chưa thanh toán</option>
                                                 <option value="Xóa">Thanh toán còn thiếu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div class="modal fade" id="addPays">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm thanh toán</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">itsweb</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số tiền thanh toán</span>
                                          </div>
                                          <div class="col-md-8">
                                            <input type="text" class="form-control" value="12000" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>HÌnh thức thanh toán</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="">ngân hàng</option>
                                                 <option value="">tiền mặt</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">Đã thanh toán</option>
                                                 <option value="Chỉnh sửa">Chưa thanh toán</option>
                                                 <option value="Xóa">Thanh toán còn thiếu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div class="modal fade" id="addSupport" style="padding-right: 19px;" aria-modal="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0">
                     <div class="card-header">
                        <h4 class="card-title">Thêm hỗ trợ</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form class="form form-horizontal">
                              <div class="form-body">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Tên dịch vụ</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">itsweb</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn ngày tạo</span>
                                          </div>
                                          <div class="col-md-8"><input class="form-control" type="date">
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn ngày kết thúc</span>
                                          </div>
                                          <div class="col-md-8">
                          <input class="form-control" type="date">                   
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">thanh toán</option>
                                                 <option value="Chỉnh sửa">đang chờ</option>
                                                 <option value="Xóa">đang xử lý</option><option value="Xóa">hủy</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                       <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div class="modal fade" id="editPay" style="padding-right: 16px; padding-left: 16px;" aria-modal="true">
       <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
                </button>
             </div>
             <div class="modal-body">
                <div class="card m-0">
                   <div class="card-header">
                      <h4 class="card-title">Chỉnh sửa thanh toán</h4>
                   </div>
                   <div class="card-content">
                      <div class="card-body">
                         <form class="form form-horizontal">
                            <div class="form-body">
                               <div class="row">
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Số tiền</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="number" value="12000" class="form-control" id="disabledInput">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Ngày thanh toán</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="date" class="form-control" id="">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Hình thức thanh toán</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="">ngân hàng</option>
                                              <option value="">tiền mặt</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Người tạo đơn</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="" value="abc">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Trạng thái</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">Xét duyệt</option>
                                              <option value="Chỉnh sửa">chưa xét duyệt</option>
                                              <option value="Xóa">Hủy</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Nghi chú</span>
                                        </div>
                                        <div class="col-md-8">
                                           <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                           </fieldset>
                                        </div>
                                     </div>
                                  </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                               </div>
                            </div>
                         </form>
                      </div>
                      <!-- end card-body -->
                   </div>
                </div>
                <!-- end card -->
             </div>
             <!-- end modal body -->
          </div>
          <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div class="modal fade" id="editSupport" style="padding-right: 16px;" aria-modal="true">
       <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
                </button>
             </div>
             <div class="modal-body">
                <div class="card m-0">
                   <div class="card-header">
                      <h4 class="card-title">Chỉnh sửa hỗ trợ</h4>
                   </div>
                   <div class="card-content">
                      <div class="card-body">
                         <form class="form form-horizontal">
                            <div class="form-body">
                               <div class="row">
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Tên dịch vụ</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">itsweb</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Chọn ngày tạo</span>
                                        </div>
                                        <div class="col-md-8"><input class="form-control" type="date">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Chọn ngày kết thúc</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input class="form-control" type="date">                   
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Trạng thái</span>
                                        </div>
                                        <div class="col-md-8">
                                           <select class="custom-select form-control" id="location1" name="location">
                                              <option value="Gửi mail">thanh toán</option>
                                              <option value="Chỉnh sửa">đang chờ</option>
                                              <option value="Xóa">đang xử lý</option>
                                              <option value="Xóa">hủy</option>
                                           </select>
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Email</span>
                                        </div>
                                        <div class="col-md-8">
                                           <input type="text" value="itsweb" class="form-control" id="">
                                        </div>
                                     </div>
                                  </div>
                                  <div class="col-12">
                                     <div class="form-group row">
                                        <div class="col-md-4">
                                           <span>Nghi chú</span>
                                        </div>
                                        <div class="col-md-8">
                                           <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                           </fieldset>
                                        </div>
                                     </div>
                                  </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                               </div>
                            </div>
                         </form>
                      </div>
                      <!-- end card-body -->
                   </div>
                </div>
                <!-- end card -->
             </div>
             <!-- end modal body -->
          </div>
          <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <div class="modal fade" id="editOrder" style="padding-right: 16px; padding-left: 16px;" aria-modal="true">
         <div class="modal-dialog" role="document" data-select2-id="15">
            <div class="modal-content" data-select2-id="14">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                     <span class="sr-only">Close</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="card m-0" data-select2-id="13">
                     <div class="card-header">
                        <h4 class="card-title">Chỉnh sửa dịch vụ</h4>
                     </div>
                     <div class="card-content" data-select2-id="12">
                        <div class="card-body" data-select2-id="11">
                           <form class="form form-horizontal" data-select2-id="10">
                              <div class="form-body" data-select2-id="9">
                                 <div class="row" data-select2-id="8">
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Mã đơn hàng</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="text" value="itsweb" class="form-control" id="disabledInput" disabled="">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12" data-select2-id="7">
                                       <div class="form-group row" data-select2-id="6">
                                           <div class="col-md-4">
                                              <span>Loại hình dịch vụ</span>
                                           </div>
                                           <div class="col-md-8" data-select2-id="9">
                                              <div class="" data-select2-id="5">
                                                 <div class="form-group mb-0" data-select2-id="8">
                                                    <select class="select2 form-control select2-hidden-accessible" multiple="" data-select2-id="34" tabindex="-1" aria-hidden="true">
                                                       <option value="cho thuê" data-select2-id="52">cho thuê</option>
                                                       <option value="bán" selected="" data-select2-id="62">bán</option>
                                                    </select>
                                                    <span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" data-select2-id="21" style="width: 1px;">
                                                       <span class="selection">
                                                          <span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                                             <ul class="select2-selection__rendered">
                                                                <li class="select2-selection__choice" title="bán" data-select2-id="2"><span class="select2-selection__choice__remove" role="presentation">×</span>bán</li>
                                                                <li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>
                                                             </ul>
                                                          </span>
                                                       </span>
                                                       <span class="dropdown-wrapper" aria-hidden="true"></span>
                                                    </span>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn số tháng thuê</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="contact-info" class="form-control" name="contact" placeholder="chọn số tháng">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Chọn ngày đăng ký</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="date" id="date" class="form-control" name="date" placeholder="chọn ngày đăng ký">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Số tiền</span>
                                          </div>
                                          <div class="col-md-8">
                                             <input type="number" id="number" class="form-control" name="number" placeholder="chọn số tiền">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Trạng thái</span>
                                          </div>
                                          <div class="col-md-8">
                                             <select class="custom-select form-control" id="location1" name="location">
                                                 <option value="Gửi mail">Đã thanh toán</option>
                                                 <option value="Chỉnh sửa">Chưa thanh toán</option>
                                                 <option value="Xóa">Thanh toán còn thiếu</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12">
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <span>Nghi chú</span>
                                          </div>
                                          <div class="col-md-8">
                                             <fieldset class="form-group">
                                              <textarea class="form-control" id="basicTextarea" rows="3" placeholder="nhập nghi chú"></textarea>
                                          </fieldset>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-8 offset-md-4 d-flex justify-content-end">
                                       <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Thêm</button>
                                       <button type="button" class="btn btn-danger waves-effect waves-light mb-1" data-dismiss="modal">Hủy</button>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div> <!-- end card-body -->
                     </div>
                  </div> <!-- end card -->
               </div> <!-- end modal body -->
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

    </div>
   <!-- BEGIN: Vendor JS-->
   <script src="./app-assets/vendors/js/vendors.min.js"></script>
   <!-- BEGIN Vendor JS-->

   <!-- BEGIN: Page Vendor JS-->
   <script src="./app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
   <script src="./app-assets/js/core/app-menu.js"></script>
   <script src="./app-assets/js/core/app.js"></script>
   <script src="./app-assets/js/scripts/components.js"></script>
   <!-- END: Theme JS-->

   <!-- BEGIN: Page JS-->
   <script src="./app-assets/js/scripts/forms/select/form-select2.js"></script>
   <!-- END: Page JS-->