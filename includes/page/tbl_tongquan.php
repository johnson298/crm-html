<section id="dashboard-analytics">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
         <div class="card bg-analytics text-white">
            <div class="card-content">
               <div class="card-body text-center">
                  <img src="./app-assets/images/elements/decore-left.png" class="img-left" alt="
                     card-img-left">
                  <img src="./app-assets/images/elements/decore-right.png" class="img-right" alt="
                     card-img-right">
                  <div class="avatar avatar-xl bg-primary shadow mt-0">
                     <div class="avatar-content">
                        <i class="feather icon-award white font-large-1"></i>
                     </div>
                  </div>
                  <div class="text-center">
                     <h1 class="mb-2 text-white">Congratulations John,</h1>
                     <p class="m-auto w-75">You have done <strong>57.6%</strong> more sales today. Check your new badge in your profile.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3 col-md-6 col-12">
         <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
               <div class="avatar bg-rgba-primary p-50 m-0">
                  <div class="avatar-content">
                     <i class="feather icon-users text-primary font-medium-5"></i>
                  </div>
               </div>
               <h2 class="text-bold-700 mt-1 mb-25">92.6k</h2>
               <p class="mb-0">Tổng tiền</p>
            </div>
            <div class="card-content" style="position: relative;">
               <div id="subscribe-gain-chart" style="min-height: 100px;">
                  <div id="apexcharts8bcvo2o3" class="apexcharts-canvas apexcharts8bcvo2o3 light" style="width: 278px; height: 100px;">
                     <svg id="SvgjsSvg1584" width="278" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;">
                        <g id="SvgjsG1586" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)">
                           <defs id="SvgjsDefs1585">
                              <clipPath id="gridRectMask8bcvo2o3">
                                 <rect id="SvgjsRect1590" width="280.5" height="102.5" x="-1.25" y="-1.25" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <clipPath id="gridRectMarkerMask8bcvo2o3">
                                 <rect id="SvgjsRect1591" width="318" height="140" x="-20" y="-20" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <linearGradient id="SvgjsLinearGradient1597" x1="0" y1="0" x2="0" y2="1">
                                 <stop id="SvgjsStop1598" stop-opacity="0.7" stop-color="rgba(115,103,240,0.7)" offset="0"></stop>
                                 <stop id="SvgjsStop1599" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="0.8"></stop>
                                 <stop id="SvgjsStop1600" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="1"></stop>
                              </linearGradient>
                           </defs>
                           <line id="SvgjsLine1589" x1="277.5" y1="0" x2="277.5" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="277.5" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line>
                           <g id="SvgjsG1603" class="apexcharts-xaxis" transform="translate(0, 0)">
                              <g id="SvgjsG1604" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g>
                           </g>
                           <g id="SvgjsG1607" class="apexcharts-grid">
                              <line id="SvgjsLine1609" x1="0" y1="100" x2="278" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                              <line id="SvgjsLine1608" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                           </g>
                           <g id="SvgjsG1593" class="apexcharts-area-series apexcharts-plot-series">
                              <g id="SvgjsG1594" class="apexcharts-series Subscribers" data:longestseries="true" rel="1" data:realindex="0">
                                 <path id="apexcharts-area-0" d="M 0 100L 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5C 278 22.5 278 22.5 278 100M 278 22.5z" fill="url(#SvgjsLinearGradient1597)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask8bcvo2o3)" pathto="M 0 100L 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5C 278 22.5 278 22.5 278 100M 278 22.5z" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <path id="apexcharts-area-0" d="M 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5" fill="none" fill-opacity="1" stroke="#7367f0" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask8bcvo2o3)" pathto="M 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <g id="SvgjsG1595" class="apexcharts-series-markers-wrap">
                                    <g class="apexcharts-series-markers">
                                       <circle id="SvgjsCircle1615" r="0" cx="278" cy="22.5" class="apexcharts-marker wi2uknpvu no-pointer-events" stroke="#ffffff" fill="#7367f0" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle>
                                    </g>
                                 </g>
                                 <g id="SvgjsG1596" class="apexcharts-datalabels"></g>
                              </g>
                           </g>
                           <line id="SvgjsLine1610" x1="0" y1="0" x2="278" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line>
                           <line id="SvgjsLine1611" x1="0" y1="0" x2="278" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                           <g id="SvgjsG1612" class="apexcharts-yaxis-annotations"></g>
                           <g id="SvgjsG1613" class="apexcharts-xaxis-annotations"></g>
                           <g id="SvgjsG1614" class="apexcharts-point-annotations"></g>
                        </g>
                        <rect id="SvgjsRect1588" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                        <g id="SvgjsG1605" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)">
                           <g id="SvgjsG1606" class="apexcharts-yaxis-texts-g"></g>
                        </g>
                     </svg>
                     <div class="apexcharts-legend"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3 col-md-6 col-12">
         <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
               <div class="avatar bg-rgba-warning p-50 m-0">
                  <div class="avatar-content">
                     <i class="feather icon-package text-warning font-medium-5"></i>
                  </div>
               </div>
               <h2 class="text-bold-700 mt-1 mb-25">97.5K</h2>
               <p class="mb-0">Đã thu</p>
            </div>
            <div class="card-content" style="position: relative;">
               <div id="orders-received-chart" style="min-height: 100px;">
                  <div id="apexchartsii6c99gn" class="apexcharts-canvas apexchartsii6c99gn light" style="width: 278px; height: 100px;">
                     <svg id="SvgjsSvg1619" width="278" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;">
                        <g id="SvgjsG1621" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)">
                           <defs id="SvgjsDefs1620">
                              <clipPath id="gridRectMaskii6c99gn">
                                 <rect id="SvgjsRect1625" width="280.5" height="102.5" x="-1.25" y="-1.25" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <clipPath id="gridRectMarkerMaskii6c99gn">
                                 <rect id="SvgjsRect1626" width="318" height="140" x="-20" y="-20" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <linearGradient id="SvgjsLinearGradient1632" x1="0" y1="0" x2="0" y2="1">
                                 <stop id="SvgjsStop1633" stop-opacity="0.7" stop-color="rgba(255,159,67,0.7)" offset="0"></stop>
                                 <stop id="SvgjsStop1634" stop-opacity="0.5" stop-color="rgba(255,245,236,0.5)" offset="0.8"></stop>
                                 <stop id="SvgjsStop1635" stop-opacity="0.5" stop-color="rgba(255,245,236,0.5)" offset="1"></stop>
                              </linearGradient>
                           </defs>
                           <line id="SvgjsLine1624" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line>
                           <g id="SvgjsG1638" class="apexcharts-xaxis" transform="translate(0, 0)">
                              <g id="SvgjsG1639" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g>
                           </g>
                           <g id="SvgjsG1642" class="apexcharts-grid">
                              <line id="SvgjsLine1644" x1="0" y1="100" x2="278" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                              <line id="SvgjsLine1643" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                           </g>
                           <g id="SvgjsG1628" class="apexcharts-area-series apexcharts-plot-series">
                              <g id="SvgjsG1629" class="apexcharts-series Orders" data:longestseries="true" rel="1" data:realindex="0">
                                 <path id="apexcharts-area-0" d="M 0 100L 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80C 278 80 278 80 278 100M 278 80z" fill="url(#SvgjsLinearGradient1632)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskii6c99gn)" pathto="M 0 100L 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80C 278 80 278 80 278 100M 278 80z" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <path id="apexcharts-area-0" d="M 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80" fill="none" fill-opacity="1" stroke="#ff9f43" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskii6c99gn)" pathto="M 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <g id="SvgjsG1630" class="apexcharts-series-markers-wrap">
                                    <g class="apexcharts-series-markers">
                                       <circle id="SvgjsCircle1650" r="0" cx="0" cy="0" class="apexcharts-marker wirt2fz9q no-pointer-events" stroke="#ffffff" fill="#ff9f43" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle>
                                    </g>
                                 </g>
                                 <g id="SvgjsG1631" class="apexcharts-datalabels"></g>
                              </g>
                           </g>
                           <line id="SvgjsLine1645" x1="0" y1="0" x2="278" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line>
                           <line id="SvgjsLine1646" x1="0" y1="0" x2="278" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                           <g id="SvgjsG1647" class="apexcharts-yaxis-annotations"></g>
                           <g id="SvgjsG1648" class="apexcharts-xaxis-annotations"></g>
                           <g id="SvgjsG1649" class="apexcharts-point-annotations"></g>
                        </g>
                        <rect id="SvgjsRect1623" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                        <g id="SvgjsG1640" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)">
                           <g id="SvgjsG1641" class="apexcharts-yaxis-texts-g"></g>
                        </g>
                     </svg>
                     <div class="apexcharts-legend"></div>
                     <div class="apexcharts-tooltip light">
                        <div class="apexcharts-tooltip-series-group">
                           <span class="apexcharts-tooltip-marker" style="background-color: rgb(255, 159, 67);"></span>
                           <div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                              <div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div>
                              <div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3 col-md-6 col-12">
         <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
               <div class="avatar bg-rgba-primary p-50 m-0">
                  <div class="avatar-content">
                     <i class="feather icon-users text-primary font-medium-5"></i>
                  </div>
               </div>
               <h2 class="text-bold-700 mt-1 mb-25">92.6k</h2>
               <p class="mb-0">Đã chi</p>
            </div>
            <div class="card-content" style="position: relative;">
               <div id="subscribe-gain-chart" style="min-height: 100px;">
                  <div id="apexcharts8bcvo2o3" class="apexcharts-canvas apexcharts8bcvo2o3 light" style="width: 278px; height: 100px;">
                     <svg id="SvgjsSvg1584" width="278" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;">
                        <g id="SvgjsG1586" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)">
                           <defs id="SvgjsDefs1585">
                              <clipPath id="gridRectMask8bcvo2o3">
                                 <rect id="SvgjsRect1590" width="280.5" height="102.5" x="-1.25" y="-1.25" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <clipPath id="gridRectMarkerMask8bcvo2o3">
                                 <rect id="SvgjsRect1591" width="318" height="140" x="-20" y="-20" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <linearGradient id="SvgjsLinearGradient1597" x1="0" y1="0" x2="0" y2="1">
                                 <stop id="SvgjsStop1598" stop-opacity="0.7" stop-color="rgba(115,103,240,0.7)" offset="0"></stop>
                                 <stop id="SvgjsStop1599" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="0.8"></stop>
                                 <stop id="SvgjsStop1600" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="1"></stop>
                              </linearGradient>
                           </defs>
                           <line id="SvgjsLine1589" x1="277.5" y1="0" x2="277.5" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="277.5" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line>
                           <g id="SvgjsG1603" class="apexcharts-xaxis" transform="translate(0, 0)">
                              <g id="SvgjsG1604" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g>
                           </g>
                           <g id="SvgjsG1607" class="apexcharts-grid">
                              <line id="SvgjsLine1609" x1="0" y1="100" x2="278" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                              <line id="SvgjsLine1608" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                           </g>
                           <g id="SvgjsG1593" class="apexcharts-area-series apexcharts-plot-series">
                              <g id="SvgjsG1594" class="apexcharts-series Subscribers" data:longestseries="true" rel="1" data:realindex="0">
                                 <path id="apexcharts-area-0" d="M 0 100L 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5C 278 22.5 278 22.5 278 100M 278 22.5z" fill="url(#SvgjsLinearGradient1597)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask8bcvo2o3)" pathto="M 0 100L 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5C 278 22.5 278 22.5 278 100M 278 22.5z" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <path id="apexcharts-area-0" d="M 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5" fill="none" fill-opacity="1" stroke="#7367f0" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask8bcvo2o3)" pathto="M 0 90C 16.216666666666665 90 30.116666666666664 60 46.33333333333333 60C 62.55 60 76.44999999999999 70 92.66666666666666 70C 108.88333333333333 70 122.78333333333333 30 139 30C 155.21666666666667 30 169.11666666666665 65 185.33333333333331 65C 201.54999999999998 65 215.45 10 231.66666666666666 10C 247.88333333333333 10 261.7833333333333 22.5 278 22.5" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <g id="SvgjsG1595" class="apexcharts-series-markers-wrap">
                                    <g class="apexcharts-series-markers">
                                       <circle id="SvgjsCircle1615" r="0" cx="278" cy="22.5" class="apexcharts-marker wi2uknpvu no-pointer-events" stroke="#ffffff" fill="#7367f0" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle>
                                    </g>
                                 </g>
                                 <g id="SvgjsG1596" class="apexcharts-datalabels"></g>
                              </g>
                           </g>
                           <line id="SvgjsLine1610" x1="0" y1="0" x2="278" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line>
                           <line id="SvgjsLine1611" x1="0" y1="0" x2="278" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                           <g id="SvgjsG1612" class="apexcharts-yaxis-annotations"></g>
                           <g id="SvgjsG1613" class="apexcharts-xaxis-annotations"></g>
                           <g id="SvgjsG1614" class="apexcharts-point-annotations"></g>
                        </g>
                        <rect id="SvgjsRect1588" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                        <g id="SvgjsG1605" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)">
                           <g id="SvgjsG1606" class="apexcharts-yaxis-texts-g"></g>
                        </g>
                     </svg>
                     <div class="apexcharts-legend"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3 col-md-6 col-12">
         <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
               <div class="avatar bg-rgba-warning p-50 m-0">
                  <div class="avatar-content">
                     <i class="feather icon-package text-warning font-medium-5"></i>
                  </div>
               </div>
               <h2 class="text-bold-700 mt-1 mb-25">97.5K</h2>
               <p class="mb-0">Công nợ</p>
            </div>
            <div class="card-content" style="position: relative;">
               <div id="orders-received-chart" style="min-height: 100px;">
                  <div id="apexchartsii6c99gn" class="apexcharts-canvas apexchartsii6c99gn light" style="width: 278px; height: 100px;">
                     <svg id="SvgjsSvg1619" width="278" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;">
                        <g id="SvgjsG1621" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)">
                           <defs id="SvgjsDefs1620">
                              <clipPath id="gridRectMaskii6c99gn">
                                 <rect id="SvgjsRect1625" width="280.5" height="102.5" x="-1.25" y="-1.25" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <clipPath id="gridRectMarkerMaskii6c99gn">
                                 <rect id="SvgjsRect1626" width="318" height="140" x="-20" y="-20" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                              </clipPath>
                              <linearGradient id="SvgjsLinearGradient1632" x1="0" y1="0" x2="0" y2="1">
                                 <stop id="SvgjsStop1633" stop-opacity="0.7" stop-color="rgba(255,159,67,0.7)" offset="0"></stop>
                                 <stop id="SvgjsStop1634" stop-opacity="0.5" stop-color="rgba(255,245,236,0.5)" offset="0.8"></stop>
                                 <stop id="SvgjsStop1635" stop-opacity="0.5" stop-color="rgba(255,245,236,0.5)" offset="1"></stop>
                              </linearGradient>
                           </defs>
                           <line id="SvgjsLine1624" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line>
                           <g id="SvgjsG1638" class="apexcharts-xaxis" transform="translate(0, 0)">
                              <g id="SvgjsG1639" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g>
                           </g>
                           <g id="SvgjsG1642" class="apexcharts-grid">
                              <line id="SvgjsLine1644" x1="0" y1="100" x2="278" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                              <line id="SvgjsLine1643" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line>
                           </g>
                           <g id="SvgjsG1628" class="apexcharts-area-series apexcharts-plot-series">
                              <g id="SvgjsG1629" class="apexcharts-series Orders" data:longestseries="true" rel="1" data:realindex="0">
                                 <path id="apexcharts-area-0" d="M 0 100L 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80C 278 80 278 80 278 100M 278 80z" fill="url(#SvgjsLinearGradient1632)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskii6c99gn)" pathto="M 0 100L 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80C 278 80 278 80 278 100M 278 80z" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <path id="apexcharts-area-0" d="M 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80" fill="none" fill-opacity="1" stroke="#ff9f43" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskii6c99gn)" pathto="M 0 60C 16.216666666666665 60 30.116666666666664 10 46.33333333333333 10C 62.55 10 76.44999999999999 80 92.66666666666666 80C 108.88333333333333 80 122.78333333333333 10 139 10C 155.21666666666667 10 169.11666666666665 90 185.33333333333331 90C 201.54999999999998 90 215.45 40 231.66666666666666 40C 247.88333333333333 40 261.7833333333333 80 278 80" pathfrom="M -1 160L -1 160L 46.33333333333333 160L 92.66666666666666 160L 139 160L 185.33333333333331 160L 231.66666666666666 160L 278 160"></path>
                                 <g id="SvgjsG1630" class="apexcharts-series-markers-wrap">
                                    <g class="apexcharts-series-markers">
                                       <circle id="SvgjsCircle1650" r="0" cx="0" cy="0" class="apexcharts-marker wirt2fz9q no-pointer-events" stroke="#ffffff" fill="#ff9f43" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle>
                                    </g>
                                 </g>
                                 <g id="SvgjsG1631" class="apexcharts-datalabels"></g>
                              </g>
                           </g>
                           <line id="SvgjsLine1645" x1="0" y1="0" x2="278" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line>
                           <line id="SvgjsLine1646" x1="0" y1="0" x2="278" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                           <g id="SvgjsG1647" class="apexcharts-yaxis-annotations"></g>
                           <g id="SvgjsG1648" class="apexcharts-xaxis-annotations"></g>
                           <g id="SvgjsG1649" class="apexcharts-point-annotations"></g>
                        </g>
                        <rect id="SvgjsRect1623" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect>
                        <g id="SvgjsG1640" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)">
                           <g id="SvgjsG1641" class="apexcharts-yaxis-texts-g"></g>
                        </g>
                     </svg>
                     <div class="apexcharts-legend"></div>
                     <div class="apexcharts-tooltip light">
                        <div class="apexcharts-tooltip-series-group">
                           <span class="apexcharts-tooltip-marker" style="background-color: rgb(255, 159, 67);"></span>
                           <div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                              <div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div>
                              <div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>