$(document).ready(function() {
	var hasSub = $('.nav-item.has-sub');
	for (var i = 0; i < hasSub.length; i++) {
		hasSub[i].onclick = function(event) {
			event.preventDefault()
			var menuContent = $(this).find('.menu-content');
			menuContent.toggleClass('active')
		};
	}

	// khach hang page
	$( "#add" ).click(function() {

		var newElement = '<tr><td><input type="text" value="" name="id[]" placeholder="4.."/></td><td><input type="text" value="" name="purpose[]" placeholder="Why do you buy this car?"/></td><td><input type="number" value="" name="amount[]" placeholder="0"/></td><td><input type="text" value="" name="currency[]" placeholder="EUR"/></td><td><input type="number" value="" name="ratio[]" placeholder="1"/></td></tr>';
		$( "#mytable" ).append( $(newElement) );

	});
  var countTD = 0;
	$( "#addChamSoc" ).click(function() {
    countTD++;
		var ghichu = $('#ghichu').val();
		var ngaytao = $('#ngaytao').val();
		var nguoitao = $('#nguoitao').val();
		var newChamSoc = "";

		newChamSoc += "<tr>";
		newChamSoc += "<td>" + countTD + "</td>";
    newChamSoc += "<td>" + ghichu + "</td>";
		newChamSoc += "<td>" + ngaytao + "</td>";
		newChamSoc += "<td>" + nguoitao + "</td>";
		newChamSoc += "<td>";
		newChamSoc += "<button class='btn btn-default deleteChamSoc'><i class='fa fa-trash-o'></i>'</button>";
		newChamSoc += "</td></tr>";

		$( "#tbl_chamsoc" ).append( $(newChamSoc) );

		$('#ghichu').val("");
		$('#ngaytao').val("");
		$('#nguoitao').val("");
	});

	$(document).on('click', '.deleteChamSoc', function () {
		$(this).closest('tr').remove();
		return false;
	});
});
document.addEventListener('DOMContentLoaded',function(){
	var toggler = document.getElementsByClassName("caret");
	var i;

	for (i = 0; i < toggler.length; i++) {
		toggler[i].addEventListener("click", function() {
			this.parentElement.querySelector(".nested").classList.toggle("active");
			this.classList.toggle("caret-down");
		});
	}

	// san pham page

	// add new product
 $('#takeCareOf').hide();
 $('#quantityMonth').hide();
 $('#tbl_infoPro').hide();
 $('#priceRent').hide();

 $('input:radio[name=loaihinh]').change(function(event) {
  var valueLoaiHinh = $(this).attr('value');
  $('#loaihinh').val(valueLoaiHinh);
  if (valueLoaiHinh == "Cho thuê") {
    $('#takeCareOf').slideDown(200);
    $('#quantityMonth').hide(); 
    $('#tbl_infoPro').hide();
    $('#priceRent').hide();
  }
  if (valueLoaiHinh == "Theo yêu cầu") {
    $('#takeCareOf').hide();
    $('#quantityMonth').hide();
    $('#tbl_infoPro').hide();
    $('#priceRent').slideDown(200);
  }
  if (valueLoaiHinh == "Chăm sóc theo tháng") {
    $('#quantityMonth').slideDown(200);
    $('#takeCareOf').hide();
    $('#tbl_infoPro').slideDown(200);
    $('#priceRent').hide();
    var nameService = $('#nameService').val();
    $('#innerNameService').text(nameService);
  }
});
 $('input:radio[name=loaihinh]').trigger('change');

    // add info product popup
    $('#newPro').click(function(event) {
      event.preventDefault();
      var nameService = $('#nameService').val();
      var notePro = $('#notePro').val();
      var loaihinh = $('#loaihinh').val();
      var quantitySer = $('#quantitySer').val();

      var htmlPro = "";

      htmlPro += "<tr>";
      htmlPro += "<td>" + nameService + "</td>";
      htmlPro += "<td>" + loaihinh + "</td>";
      htmlPro += "<td>" + quantitySer + "</td>";
      htmlPro += "<td>" + notePro + "</td>";
      htmlPro += "</tr>";

      $( '#tbl_infoPro_show' ).append($(htmlPro));
    });

    // only checked in checkbox (priceOf)
    $('input[type=checkbox]').on('click', function(event) {
      if ($(this).is(':checked')) {
        var group = "input:checkbox[name='" + $(this).attr('name'); + "']"
      }
    });

    // tooltip
    $('[data-toggle="tooltip"]').tooltip(); 

    // check box all
    $("#checkedAll").change(function(){
      if(this.checked){
        $(".checkSingle").each(function(){
          this.checked=true;
        })              
      }else{
        $(".checkSingle").each(function(){
          this.checked=false;
        })              
      }
    });
    $(".checkedAll_class").change(function(){
      if(this.checked){
        $(".checkSingle").each(function(){
          this.checked=true;
        })              
      }else{
        $(".checkSingle").each(function(){
          this.checked=false;
        })              
      }
    });

    $(".checkSingle").click(function () {
      if ($(this).is(":checked")){
        var isAllChecked = 0;
        $(".checkSingle").each(function(){
          if(!this.checked)
           isAllChecked = 1;
       })              
        if(isAllChecked == 0){ 
          $("#checkedAll").prop("checked", true); 
          $(".checkedAll_class").prop("checked", true); 
        }     
      }else {
        $("#checkedAll").prop("checked", false);
        $(".checkedAll_class").prop("checked", false);
      }
    });

    $('#remove-sorting').removeClass('sorting_asc');
    $('#remove-sorting').on('click', function(event) {
      $(this).removeClass('sorting_desc').removeClass('sorting_asc')
    });

  // end check box all
})
